﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebServiceClient
{
    public class Response
    {
        public string CountryCode { get; set; }
        public string VatNumber { get; set; }
        public string TraderName { get; set; }
        public string TraderAddress { get; set; }
        public string RequestDate { get; set; }
        public string RequestIdentifier { get; set; }
        public bool Valid { get; set; }
    }
}
