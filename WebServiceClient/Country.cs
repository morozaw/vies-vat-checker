﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebServiceClient
{
    class Country
    {
        public string CountryShortName { get; set; }
        public string CountryName { get; set; }
    }
}
