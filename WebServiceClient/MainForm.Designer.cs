﻿namespace WebServiceClient
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.output_textBox = new System.Windows.Forms.RichTextBox();
            this.country_label = new System.Windows.Forms.Label();
            this.vatNumber_label = new System.Windows.Forms.Label();
            this.vatNumber_textBox = new System.Windows.Forms.TextBox();
            this.checkVat_button = new System.Windows.Forms.Button();
            this.checkVatApprox_button = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.requesterVatNumber_textBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.countryCode_comboBox = new System.Windows.Forms.ComboBox();
            this.requesterCountryCode_comboBox = new System.Windows.Forms.ComboBox();
            this.saveXML_button = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
            // 
            // output_textBox
            // 
            this.output_textBox.Location = new System.Drawing.Point(12, 175);
            this.output_textBox.Name = "output_textBox";
            this.output_textBox.Size = new System.Drawing.Size(314, 178);
            this.output_textBox.TabIndex = 1;
            this.output_textBox.Text = "";
            // 
            // country_label
            // 
            this.country_label.AutoSize = true;
            this.country_label.Location = new System.Drawing.Point(12, 9);
            this.country_label.Name = "country_label";
            this.country_label.Size = new System.Drawing.Size(118, 13);
            this.country_label.TabIndex = 2;
            this.country_label.Text = "Państwo Członkowskie";
            // 
            // vatNumber_label
            // 
            this.vatNumber_label.AutoSize = true;
            this.vatNumber_label.Location = new System.Drawing.Point(12, 48);
            this.vatNumber_label.Name = "vatNumber_label";
            this.vatNumber_label.Size = new System.Drawing.Size(62, 13);
            this.vatNumber_label.TabIndex = 6;
            this.vatNumber_label.Text = "Numer VAT";
            // 
            // vatNumber_textBox
            // 
            this.vatNumber_textBox.Location = new System.Drawing.Point(15, 64);
            this.vatNumber_textBox.Name = "vatNumber_textBox";
            this.vatNumber_textBox.Size = new System.Drawing.Size(100, 20);
            this.vatNumber_textBox.TabIndex = 5;
            this.vatNumber_textBox.Text = "6342709934";
            // 
            // checkVat_button
            // 
            this.checkVat_button.Location = new System.Drawing.Point(224, 146);
            this.checkVat_button.Name = "checkVat_button";
            this.checkVat_button.Size = new System.Drawing.Size(102, 23);
            this.checkVat_button.TabIndex = 7;
            this.checkVat_button.Text = "checkVat";
            this.checkVat_button.UseVisualStyleBackColor = true;
            this.checkVat_button.Click += new System.EventHandler(this.checkVat_button_Click);
            // 
            // checkVatApprox_button
            // 
            this.checkVatApprox_button.Location = new System.Drawing.Point(121, 146);
            this.checkVatApprox_button.Name = "checkVatApprox_button";
            this.checkVatApprox_button.Size = new System.Drawing.Size(102, 23);
            this.checkVatApprox_button.TabIndex = 8;
            this.checkVatApprox_button.Text = "checkVatApprox";
            this.checkVatApprox_button.UseVisualStyleBackColor = true;
            this.checkVatApprox_button.Click += new System.EventHandler(this.checkVatApprox_button_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 133);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Numer VAT";
            // 
            // requesterVatNumber_textBox
            // 
            this.requesterVatNumber_textBox.Location = new System.Drawing.Point(15, 149);
            this.requesterVatNumber_textBox.Name = "requesterVatNumber_textBox";
            this.requesterVatNumber_textBox.Size = new System.Drawing.Size(100, 20);
            this.requesterVatNumber_textBox.TabIndex = 11;
            this.requesterVatNumber_textBox.Text = "6342709934";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(173, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Państwo Członkowskie pytającego";
            // 
            // countryCode_comboBox
            // 
            this.countryCode_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.countryCode_comboBox.FormattingEnabled = true;
            this.countryCode_comboBox.Location = new System.Drawing.Point(15, 24);
            this.countryCode_comboBox.Name = "countryCode_comboBox";
            this.countryCode_comboBox.Size = new System.Drawing.Size(121, 21);
            this.countryCode_comboBox.TabIndex = 13;
            // 
            // requesterCountryCode_comboBox
            // 
            this.requesterCountryCode_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.requesterCountryCode_comboBox.FormattingEnabled = true;
            this.requesterCountryCode_comboBox.Location = new System.Drawing.Point(15, 109);
            this.requesterCountryCode_comboBox.Name = "requesterCountryCode_comboBox";
            this.requesterCountryCode_comboBox.Size = new System.Drawing.Size(121, 21);
            this.requesterCountryCode_comboBox.TabIndex = 14;
            // 
            // saveXML_button
            // 
            this.saveXML_button.Enabled = false;
            this.saveXML_button.Location = new System.Drawing.Point(224, 117);
            this.saveXML_button.Name = "saveXML_button";
            this.saveXML_button.Size = new System.Drawing.Size(102, 23);
            this.saveXML_button.TabIndex = 15;
            this.saveXML_button.Text = "Save XML";
            this.saveXML_button.UseVisualStyleBackColor = true;
            this.saveXML_button.Click += new System.EventHandler(this.saveXML_button_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(338, 365);
            this.Controls.Add(this.saveXML_button);
            this.Controls.Add(this.requesterCountryCode_comboBox);
            this.Controls.Add(this.countryCode_comboBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.requesterVatNumber_textBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.checkVatApprox_button);
            this.Controls.Add(this.checkVat_button);
            this.Controls.Add(this.vatNumber_label);
            this.Controls.Add(this.vatNumber_textBox);
            this.Controls.Add(this.country_label);
            this.Controls.Add(this.output_textBox);
            this.Name = "MainForm";
            this.Text = "VAT Checker";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.RichTextBox output_textBox;
        private System.Windows.Forms.Label country_label;
        private System.Windows.Forms.Label vatNumber_label;
        private System.Windows.Forms.TextBox vatNumber_textBox;
        private System.Windows.Forms.Button checkVat_button;
        private System.Windows.Forms.Button checkVatApprox_button;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox requesterVatNumber_textBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox countryCode_comboBox;
        private System.Windows.Forms.ComboBox requesterCountryCode_comboBox;
        private System.Windows.Forms.Button saveXML_button;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}

