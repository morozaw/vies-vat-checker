﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace WebServiceClient
{
    public partial class MainForm : Form
    {
        private Response response;

        public MainForm()
        {
            this.InitializeComponent();

            List<Country> countries = new List<Country>()
                {
                    new Country() {CountryShortName = "PL", CountryName = "Polska" },
                    new Country() {CountryShortName = "AT", CountryName = "Austria" },
                    new Country() {CountryShortName = "BE", CountryName = "Belgia" },
                    new Country() {CountryShortName = "BG", CountryName = "Bułgaria" },
                    new Country() {CountryShortName = "CY", CountryName = "Cypr" },
                    new Country() {CountryShortName = "CZ", CountryName = "Republika Czeska" },
                    new Country() {CountryShortName = "DE", CountryName = "Niemcy" },
                    new Country() {CountryShortName = "DK", CountryName = "Dania" },
                    new Country() {CountryShortName = "EE", CountryName = "Estonia" },
                    new Country() {CountryShortName = "EL", CountryName = "Grecja" },
                    new Country() {CountryShortName = "ES", CountryName = "Hiszpania" },
                    new Country() {CountryShortName = "FI", CountryName = "Finlandia" },
                    new Country() {CountryShortName = "FR", CountryName = "Francja" },
                    new Country() {CountryShortName = "GB", CountryName = "Wielka Brytania" },
                    new Country() {CountryShortName = "HT", CountryName = "Chorwacja" },
                    new Country() {CountryShortName = "HU", CountryName = "Węgry" },
                    new Country() {CountryShortName = "IR", CountryName = "Irlandia" },
                    new Country() {CountryShortName = "IT", CountryName = "Włochy" },
                    new Country() {CountryShortName = "LT", CountryName = "Litwa" },
                    new Country() {CountryShortName = "LU", CountryName = "Luksemburg" },
                    new Country() {CountryShortName = "LV", CountryName = "Łotwa" },
                    new Country() {CountryShortName = "MT", CountryName = "Malta" },
                    new Country() {CountryShortName = "NL", CountryName = "Holandia" },
                    new Country() {CountryShortName = "PT", CountryName = "Portugalia" },
                    new Country() {CountryShortName = "RO", CountryName = "Rumunia" },
                    new Country() {CountryShortName = "SE", CountryName = "Szwecja" },
                    new Country() {CountryShortName = "SI", CountryName = "Słowenia" },
                    new Country() {CountryShortName = "SK", CountryName = "Słowacja" }
            };
            
            this.countryCode_comboBox.DataSource = countries;
            this.countryCode_comboBox.DisplayMember = "CountryName";
            this.countryCode_comboBox.ValueMember = "CountryShortName";

            this.requesterCountryCode_comboBox.BindingContext = new BindingContext();
            this.requesterCountryCode_comboBox.DataSource = countries;
            this.requesterCountryCode_comboBox.DisplayMember = "CountryName";
            this.requesterCountryCode_comboBox.ValueMember = "CountryShortName";
        }

        private void checkVat_button_Click(object sender, EventArgs e)
        {
            try
            {
                this.output_textBox.Clear();

                VATChecker checker = new VATChecker(this.countryCode_comboBox.SelectedValue.ToString(), this.vatNumber_textBox.Text);
                response = checker.checkVat();

                if (response.Valid)
                {
                    this.output_textBox.AppendText("Numer VAT aktywny." + Environment.NewLine);
                    this.output_textBox.AppendText("Państwo Członkowskie: " + response.CountryCode + Environment.NewLine);
                    this.output_textBox.AppendText("Numer VAT: " + response.VatNumber + Environment.NewLine);
                    this.output_textBox.AppendText("Data zapytania: " + response.RequestDate + Environment.NewLine);
                }
                else
                {
                    this.output_textBox.AppendText("Numer VAT nieaktywny." + Environment.NewLine);
                    this.output_textBox.AppendText("Państwo Członkowskie: " + response.CountryCode + Environment.NewLine);
                    this.output_textBox.AppendText("Numer VAT: " + response.VatNumber + Environment.NewLine);
                    this.output_textBox.AppendText("Data zapytania: " + response.RequestDate + Environment.NewLine);
                }
            }
            catch (Exception ex) { this.output_textBox.AppendText(ex.Message + Environment.NewLine); }
            finally { this.saveXML_button.Enabled = true; }
        }

        private void checkVatApprox_button_Click(object sender, EventArgs e)
        {
            try
            {
                this.output_textBox.Clear();
                
                VATChecker checker = new VATChecker(this.countryCode_comboBox.SelectedValue.ToString(), this.vatNumber_textBox.Text, this.countryCode_comboBox.SelectedValue.ToString(), this.requesterVatNumber_textBox.Text);
                response = checker.checkVatApprox();

                if (response.Valid)
                {
                    this.output_textBox.AppendText("Numer VAT aktywny." + Environment.NewLine);
                    this.output_textBox.AppendText("Nazwa: " + response.TraderName + Environment.NewLine);
                    this.output_textBox.AppendText("Adres: " + response.TraderAddress + Environment.NewLine);
                    this.output_textBox.AppendText("Państwo Członkowskie: " + response.CountryCode + Environment.NewLine);
                    this.output_textBox.AppendText("Numer VAT: " + response.VatNumber + Environment.NewLine);
                    this.output_textBox.AppendText("Data zapytania: " + response.RequestDate + Environment.NewLine);
                    this.output_textBox.AppendText("Identyfikator zapytania: " + response.RequestIdentifier + Environment.NewLine);
                }
                else
                {
                    this.output_textBox.AppendText("Numer VAT nieaktywny." + Environment.NewLine);
                    this.output_textBox.AppendText("Państwo Członkowskie: " + response.CountryCode + Environment.NewLine);
                    this.output_textBox.AppendText("Numer VAT: " + response.VatNumber + Environment.NewLine);
                    this.output_textBox.AppendText("Data zapytania: " + response.RequestDate + Environment.NewLine);
                }
            }
            catch (Exception ex) { this.output_textBox.AppendText("Wystąpił błąd: " + ex.Message + Environment.NewLine); }
            finally { this.saveXML_button.Enabled = true; }
        }

        private void saveXML_button_Click(object sender, EventArgs e)
        {
            saveFileDialog1.FileName = "wynik.xml";
            saveFileDialog1.Title = "Zapisz plik XML";
            saveFileDialog1.Filter = "Plik XML|*.xml";
            saveFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            saveFileDialog1.ShowDialog();
           
            using (StreamWriter myWriter = new StreamWriter(saveFileDialog1.FileName, false))
            {
                XmlSerializer mySerializer = new XmlSerializer(typeof(Response));
                mySerializer.Serialize(myWriter, response);
            }
            MessageBox.Show("Zapisano!");
        }
    }
}
