﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebServiceClient.VIES;

namespace WebServiceClient
{
    class VATChecker
    {
        private string countryCode, vatNumber, requesterCountryCode, requesterVatNuber;

        /// <summary>
        /// konstruktor
        /// </summary>
        /// <param name="countryCode">
        /// Kod kraju
        /// </param> 
        /// <param name="vatNumber">
        /// numer vat
        /// </param> 
        public VATChecker(string countryCode, string vatNumber)
        {
            this.countryCode = countryCode;
            this.vatNumber = vatNumber;
        }

        /// <summary>
        /// konstruktor
        /// </summary>
        /// <param name="countryCode">
        /// Kod kraju
        /// </param> 
        /// <param name="vatNumber">
        /// numer vat
        /// </param> 
        /// <param name="requesterCountryCode">
        /// Kod kraju pytającego
        /// </param> 
        /// <param name="requesterVatNumber">
        /// numer vat pytającego
        /// </param> 
        public VATChecker(string countryCode, string vatNumber, string requesterCountryCode, string requesterVatNumber)
        {
            this.countryCode = countryCode;
            this.vatNumber = vatNumber;
            this.requesterCountryCode = requesterCountryCode;
            this.requesterVatNuber = requesterVatNumber;
        }
        /// <summary>
        /// proste sprawdzanie, które informuje przy pomocy zmiennej valid czy NIP jest aktywny czy też nie
        /// </summary> 
        /// <param name="countryCode">
        /// Kod kraju
        /// </param>
        /// <param name="vatNumber">
        /// Numer VAT
        /// </param>  
        /// <param name="name">
        /// Nazwa firmy
        /// </param>  
        /// <param name="address">
        /// Adres firmy
        /// </param>  
        /// <param name="requestDate">
        /// Data zapytania
        /// </param>  
        /// <param name="valid">
        /// Aktywny / nieaktywny
        /// </param>  
        public Response checkVat()
        {
            try
            {
                checkVatRequestBody body = new checkVatRequestBody();
                body.countryCode = this.countryCode;
                body.vatNumber = this.vatNumber;
                checkVatRequest request = new checkVatRequest(body);

                checkVatResponse vatResponse = ((checkVatPortType)new checkVatPortTypeClient()).checkVat(request);
                
                Response response = new Response();
                response.CountryCode = vatResponse.Body.countryCode;
                response.RequestDate = vatResponse.Body.requestDate;
                response.TraderAddress = vatResponse.Body.address;
                response.TraderName = vatResponse.Body.name;
                response.Valid = vatResponse.Body.valid;
                response.VatNumber = vatResponse.Body.vatNumber;
                return response;
            }
            catch
            {
                throw;
            }
        }
        /// <summary>
        /// sprawdzanie szczegółowe, które oprócz walidacji nr NIP zwraca informacje dotyczące:
        /// nazwy firmy, adresu oraz identyfikatora transakcji, 
        /// pobiera również dane dotyczące państwa członkowskiego pytającego 
        /// można podać dane firmy Senetic: PL 6342709934
        /// </summary>
        /// <param name="countryCode">
        /// Kod kraju
        /// </param>
        /// <param name="vatNumber">
        /// Numer VAT
        /// </param>  
        /// <param name="traderName">
        /// Nazwa firmy
        /// </param>  
        /// <param name="traderAddress">
        /// Adres firmy
        /// </param>  
        /// <param name="requestDate">
        /// Data zapytania
        /// </param>  
        /// <param name="requestIdentifier">
        /// Identyfikator zapytania
        /// </param>  
        /// <param name="valid">
        /// Aktywny / nieaktywny
        /// </param>  
        public Response checkVatApprox()
        {
            try
            {
                checkVatApproxRequestBody body = new checkVatApproxRequestBody();
                body.countryCode = this.countryCode;
                body.vatNumber = this.vatNumber;
                body.requesterCountryCode = this.requesterCountryCode;
                body.requesterVatNumber = this.requesterVatNuber;

                checkVatApproxRequest request = new checkVatApproxRequest(body);
                checkVatApproxResponse vatResponse = ((checkVatPortType)new checkVatPortTypeClient()).checkVatApprox(request);

                Response response = new Response();
                response.CountryCode = vatResponse.Body.countryCode;
                response.RequestDate = vatResponse.Body.requestDate;
                response.RequestIdentifier = vatResponse.Body.requestIdentifier;
                response.TraderAddress = vatResponse.Body.traderAddress;
                response.TraderName = vatResponse.Body.traderName;
                response.Valid = vatResponse.Body.valid;
                response.VatNumber = vatResponse.Body.vatNumber;

                return response;
            }
            catch
            {
                throw;
            }
        }
    }
}